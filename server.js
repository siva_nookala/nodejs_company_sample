var express = require('express');
const Sequelize = require('sequelize');
var bodyParser = require('body-parser');
app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('port', process.env.PORT || 2017);
const connection = new Sequelize("company", "root", "admin", {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});
app.post('/createEmployee', function (req, res) {
    var company = Company.findOne({where:{name:req.body.company}}).then(companyInfo =>{
        if(companyInfo){
            Employee.findOne({where:{name:req.body.name}}).then(employee =>{
                if(employee){
                    res.send("Employee "+req.body.name+" already exist.");
                }else{
                    var employee = Employee.create({
                        name: req.body.name,
                        companyId : companyInfo.id,
                        employeeId: req.body.employeeId,
                        jobHistory:req.body.jobHistory
                    }).then(data =>{
                        Employee.findAll().then(employees => {
                        res.send(employees);
                        })
                    })
                }
            });
        }else{
            res.send("Company "+req.body.company+" not found.");
        }
    });
})

app.get('/getEmployees', function (req, res) {
    Employee.findAll({
        include: [
            { model: Employee}
        ]
    }).then(employees => {
        res.send(employees);
    });
})


app.post('/createCompany', function (req, res) {
    var company = Company.findOne({where:{name:req.body.name}}).then(companyInfo =>{
        if(companyInfo){
            //TODO NEED TO UPDATE COMPANY AND EMPLOYEES.
            companyInfo.updateAttributes({  name: req.body.name,
                taxId:req.body.taxId,
                location: req.body.location});
            res.send("The company already exists, please choose another.");
        }else{
            Company.create({
                name: req.body.name,
                taxId:req.body.taxId,
                location: req.body.location
            }).then(company =>{
                var employeesList = req.body.employees;
                if(employeesList){
                    var i,count=0;
                    for (i = 0; i < employeesList.length; i++) {
                        var employee=employeesList[i];
                        Employee.create({
                            name: employee.name,
                            companyId : company.id,
                            employeeId: employee.employeeId,
                            jobHistory:employee.jobHistory
                        }).then(data => {
                                count++;
                                if(count==employeesList.length){
                                    Company.findAll({include: [{model: Employee}]}).then(companyes => {
                                        res.send(companyes);
                                    });
                                }
                            });
                        }
                        if(employeesList.length==0) {
                            Company.findAll({include: [{model: Employee}]}).then(companyes => {
                                res.send(companyes);
                            });
                        }
                 }
            });
        }
    });
})
function createOrUpdateEmployee(company,req,res) {
    var employeesList = req.body.employees;
    if(employeesList){
        var i,count=0;
        for (i = 0; i < employeesList.length; i++) {
            var employee=employeesList[i];
            Employee.findOne({where:{companyId:company.id,employeeId:employee.employeeId}}).then(employee =>{
                if(employee){
                    employee.updateAttributes({
                        name:employee.name
                    }).then(employee =>{
                        count++;
                        if(count==employeesList.length){
                            Company.findAll({include: [{model: Employee}]}).then(companyes => {
                                res.send(companyes);
                            });
                        }
                    });
                }else{
                    var employee = Employee.create({
                        name: employee.name,
                        companyId: company.id,
                        employeeId: employee.employeeId
                    }).then(data => {
                        count++;
                        if (count == employeesList.length) {
                            Company.findAll({include: [{model: Employee}]}).then(companyes => {
                                res.send(companyes);
                        });
                        }
                    });

                }
            });
        }
        if(employeesList.length==0) {
            Company.findAll({include: [{model: Employee}]}).then(companyes => {
                res.send(companyes);
            });
        }
    }else{
        Company.findAll({include: [{model: Employee}]}).then(companyes => {
            res.send(companyes);
        });
    }
}



app.get('/getCompanies', function (req, res) {
    Company.findAll({include: [{model: Employee}]}).then(companyes => {
        res.send(companyes);
    });
})

app.get('/filter', function (req, res) {
    Company.findAll({include: [{model: Employee,where:{
                jobHistory:{
                    role: req.query.role
                }
            }
        }]}).then(companyes => {
        res.send(companyes);
});
})

app.listen(app.get('port'), function () {
    console.log("Server started...https://localhost:" + app.get('port'));
})

connection.sync({
    logging: console.log
});

const Employee = connection.define("employee", {
    name: Sequelize.STRING,
    employeeId:Sequelize.STRING,
    jobHistory:Sequelize.JSON
});

const Company = connection.define("company", {
    name: Sequelize.STRING,
    location: Sequelize.STRING,
    taxId: Sequelize.STRING
});
Company.hasMany(Employee, {foreignKey: 'companyId'});
